/**
 * scripts.js
 */


 /* ============== Page loader ============== */

 window.onload = function(){
    "use strict";
     $('.page-loader').fadeOut(500);
 };

 /* ============== Google maps ============== */


 (function($) {
    "use strict";
     /* ============== Plugins ============== */

     /* Waves effect */
     Waves.init({
         duration : 400
     });
     Waves.attach('.btn', ['waves-button']);
     Waves.attach('ul.social-icons li a', ['waves-button', 'waves-light']);


     /* Bootstrap tooltip plugin */
     $('[data-toggle="tooltip"]').tooltip({
         container : "body"
     });

     /* ============== Other js ============== */

     $(document).on('click', '.accordion-group .accordion .accordion-title', function () {
         var that = $(this);
         that.closest(".accordion-group").find('.accordion').finish().removeClass("active", 100);
         that.parent(".accordion").finish().addClass("active", 100);
         return false;
     });


     // Responsive button & overlay click
     $(document).on('click', 'nav.navbar ul.navbar-nav.responsive-btn a', function () {
         $('.navs').addClass('open animated slideInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
             $(this).removeClass('animated slideInRight');
         });
         $('body').addClass("scroll-disabled").append('<div class="body-overlay"></div>');
         return false;
     }).on('click', '.body-overlay', function () {
         var that = $(this);
         $('.navs').addClass('animated slideOutRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
             $(this).removeClass('open animated slideOutRight');
             $('body').removeClass("scroll-disabled");
             that.remove();
         });
         return false;
     });

     var window_width = $(window).width();

     $(window).resize(function(){
         window_width = $(this).width();
     });

     // Responsive sidebar menu click
     $(document).on('click', 'nav.navbar ul.navbar-nav:not(.responsive-btn) li a', function () {
         if(window_width <= 992) {
             var that = $(this);
             that.parent().siblings().find('ul').hide(200);
             if (that.next('ul').length > 0) {
                 that.next('ul').finish().slideToggle(200);
                 return false;
             }
         }
     });

     /* JavaScript Media Queries */
    if (matchMedia) {
      var mq = window.matchMedia("(max-width: 979px)");
      mq.addListener(WidthChange);
      WidthChange(mq);
    }

    // media query change
    function WidthChange(mq) {
      if (mq.matches) {
        $('#breadcrumbs').insertBefore('.site-footer');
      } else {
        $('#content').prepend($('#breadcrumbs'));
      }
    }

 })(jQuery);
